#!/bin/bash
apt-get install -y debconf-utils
echo "firebird2.5-classic shared/firebird/enabled boolean true" | debconf-set-selections
echo "firebird2.5-classic shared/firebird/sysdba_password/first_install password masterkey" | debconf-set-selections
apt-get install -y firebird2.5-classic
gsec -user SYSDBA -password masterkey -add CHEA -pw PDNTP
service openbsd-inetd restart