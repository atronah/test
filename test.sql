-- comment for test sql-procedure
-- comment with cyrillic: тестовый комментарий с текстом на русском языке
create or alter procedure test_procedure(
    input_one bigint
    , input_two varchar(16)
)
as 
begin
    /* comment in the body */
    insert into test_table (id, value) values (input_one, input_two);
end
